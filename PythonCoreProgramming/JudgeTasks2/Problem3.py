position = int(input())
array = [int(i) for i in input().split(',')]
commands = []
forward = 0
backwards = 0
n = position

while True:
    instructions = input().split()

    if  instructions[0] == 'exit':
        break
    
    steps = int(instructions[0])
    direction = instructions[1]
    size = int(instructions[2])

    for i in range(steps):
        if direction == 'forward':
            n = (n + size) % len(array)
            forward += array[n]
        elif direction == 'backwards':
            n = (n - size + len(array)) % len(array)
            backwards += array[n]

print(f'Forward: {forward}')
print(f'Backwards: {backwards}')

   

