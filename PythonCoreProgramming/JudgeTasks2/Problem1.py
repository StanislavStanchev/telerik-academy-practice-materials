word = input()
results = []
counter = 1

for i in range(len(word) - 1):
    
    if word[i] == word[i+1]:
        counter += 1
    else:
        counter = 1
        
    results.append([counter, word[i]])

def max_ocurrence(results: list):
    while len(results) > 1:
        if results[0][0] > results[1][0]:
            results.pop(1)
        elif results[0][0] < results[1][0]:
            results.pop(0)
        else:
            results.pop(1)
    return results

max_ocuurence = max_ocurrence(results)

print(max_ocuurence[0][0] * max_ocuurence[0][1])