n = int(input())

matrix = [[0 for _ in range(n)] for _ in range(n)]
row = 0
column = 0

print(matrix)


for i in range(1, n*n + 1):

    matrix[row][column] = i

    if  column + 1 < n and  matrix[row][column + 1] == 0:
        column += 1

    elif row + 1 <  n and matrix[row + 1][column] == 0:
        row += 1

    elif column > 0 and matrix[row][column - 1] == 0:
        column -= 1

    elif row > 0 and  matrix[row - 1][column] == 0:
         row -= 1


print(matrix)
