# Three Groups
# You are given an array of numbers.

# You task is to group the numbers by remainder of 3.

# Example:

# arr = {1, 2, 3, 4, 5, 6, 7}
# groups:
# 0 -> 3, 6
# 1 -> 1, 4, 7
# 2 -> 2, 5
# Input
# Read from the standard input

# On the single line, read the numbers of the array
# Separated by a whitespace
# Output
# Print on the standard output

# On the first line, print the group with remainder 0
# On the second line, print the group with remainder 1
# On the third line, print the group with remainder 2
# On each line, the numbers in a group are separated with a single whitespace
# Sample tests
# Input
# 1 2 3 4 5 6 7
# Output
# 3 6
# 1 4 7
# 2 5
# Input
# 3 3 3 3 3
# Output
# 3 3 3 3 3
# Note that there are two empty lines for the two empty groups

# numbers = [int(number) for number in input().split()]
# remainders = {}
# for n in numbers:
#     remainder = n % 3
#     if remainder in remainders:
#         remainders[n%3].append(n)
#     else:
#         remainders[remainder] = [n]

# print(remainders)


numbers = [int(number) for number in input().split()]
all = [[], [], []]

for n in numbers:
    remainder = n % 3
    if remainder == 0:
        all[0].append(n)
    elif remainder == 1:
        all[1].append(n)
    elif remainder == 2:
        all[2].append(n)

for remainders in all:
    for n in remainders:
        print(n, end=' ')
    print()