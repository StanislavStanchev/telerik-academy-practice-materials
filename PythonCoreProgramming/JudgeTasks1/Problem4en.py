# Battle Boats
# You and your best friend are so bored of writing code whole day and decide to play a game of battle boats.

# After a few games played you felt bored again and since you are already a programmer geek you decide to implement it using all your skills acquired during the last couple of weeks.

# The first player to Shoot is Player 1

# Input
# Read from the standard input
# On the first line, find R and C, separated by a space
# The size of the board of each player (they are equal)
# R rows, C columns
# On the next 2*R rows, find the numbers 1 or 0 as a value of every cell of the board of each player.
# The first R lines are for player one's and the next R lines are for player two's board.
# After that you will receive a sequence of commands
# Each on a new line until you reach END
# The commands will be in the format [Shoot R C] where:
# R is the row on which you put your bomb
# C is the column on which you put your bomb
# R and C define the cell
# Output
# Print on the standard output
# Booom
# Missed
# You already shot there!
# On the last line you should print the result of every player (boats left undestroyed) int format P1:P2
# Example
# 2 2
# 0 1
# 1 1
# 1 0
# 1 1
# Shoot 1 1
# Shoot 0 1
# Shoot 0 0
# Shoot 0 0
# Shoot 1 1
# Shoot 1 0
# END


# Output:

# Booom
# Booom
# Booom
# Missed
# You already shot there!
# Booom
# 1:1

# Sample tests
# Input
# 2 2
# 0 1
# 1 1
# 1 0
# 1 1
# Shoot 1 1
# Shoot 0 1
# Shoot 0 0
# Shoot 0 0
# Shoot 1 1
# Shoot 1 0
# END
# Output
# Booom
# Booom
# Booom
# Missed
# You already shot there!
# Booom
# 1:1
# Input
# 3 4
# 0 1 1 1
# 1 1 0 0
# 1 1 0 1
# 1 0 1 1
# 1 0 0 0
# 1 1 1 1
# Shoot 2 3
# Shoot 1 1
# Shoot 2 1
# Shoot 0 0
# Shoot 1 1
# Shoot 1 1
# Shoot 2 1
# Shoot 2 3
# END
# Output
# Booom
# Booom
# Booom
# Missed
# Missed
# You already shot there!
# You already shot there!
# Booom
# 6:6
# Input
# 2 2
# 0 0
# 1 0
# 0 1
# 0 1
# Shoot 1 1
# END
# Yes



def generate_boards(r, c):
    board= []

    for i in range(r):
        row = []
        while len(row) < c:
         row = [int(i) for i in input().split()]
        board.append(row)

    return board

def check_location(board: list, r, c):
   result = []
   location = board[r][c]
   if 2 == location:
      return 'You already shot there!'

   elif location == 1:
    
      board[r][c] = 2
      return 'Booom'
      
   else:
      return 'Missed'
   

def show_actions(results):
    for r in results:
         print(r)

def check_result(board_one, board_two):
   result_one = 0
   result_two = 0

   for r in range(len(board_one)):
      for c in range(len(board_one[0])):
         if board_one[r][c] == 1:
            result_one +=1

         if board_two[r][c] == 1:
            result_two +=1

   print(f'{result_one}:{result_two}')

def reverse_matrix(matrix):
   new_matrix = []
   for elements in matrix:
      new_matrix.insert(0,list(reversed(elements)))
   return new_matrix

def battle_boats():
  r, c = [int(i) for i in input().split()]
  board_one = generate_boards(r, c)
  board_two = reverse_matrix(generate_boards(r, c))
  
  location = ''
  results = []
  
  while True:
     location = input().split()
     if location[0] == 'END':
        break
     r = int(location[1])
     c = int(location[2])
     result = check_location(board_two, r, c)
     results.append(result)
     

     location = input().split()
     if location[0] == 'END':
        break
     ro = int(location[1])
     co = int(location[2])
     result_two = check_location(board_one, ro, co)
     results.append(result_two)

  show_actions(results)
  check_result(board_one, board_two)

battle_boats()