# Above the Main Diagonal
# Description
# You are given a number N. Using it, create a square matrix of numbers, formed by powers of 2 and find the sum of 
# the numbers above the main diagonal, excluding the diagonal itself.
# Input
# Read from the standard input.
# On the first line, read the number N - the number of rows and columns.
# Output
# Print to the standard output.
# On a single line print the sum of the numbers above the main diagonal excluding the diagonal.
# Constraints
# N can get as big as 30.
# Sample tests
# Input
# 4
# Output
# 70
# Explanation
# With N equal to 4, we will have a matrix that looks like this:

#  1  2  4  8
#  2  4  8 16
#  4  8 16 32
#  8 16 32 64
# The main diagonal is 1 4 16 64, so the sum of the numbers above it: 2 + 4 + 8 + 8 + 16 + 32 = 70.


n = int(input())
matrix = []
sum_above_diagonal = 0
j = 1 
for i in range(n):

    inmatrix = []

    for u in range(n):
        inmatrix.append(j)
        j *= 2

    matrix.append(inmatrix)
    sum_above_diagonal += sum(inmatrix[i+1:])
    j = matrix[len(matrix) - 1][1] if len(inmatrix) > 1 else matrix[len(matrix) - 1][0]

print(sum_above_diagonal)

n = int(input())
matrix = []
sum_above_diagonal = 0
for i in range(n):

    if i == 0:
     j = 1
    else:
       j = matrix[len(matrix) - 1][1]

    inmatrix = []

    for u in range(n):
        inmatrix.append(j)
        j *= 2

    matrix.append(inmatrix)
    sum_above_diagonal += sum(inmatrix[i+1:])

print(sum_above_diagonal)





