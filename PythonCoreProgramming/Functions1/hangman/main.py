def hide_word(word: list):
    
    hidden_word = word.copy()
    lenght = len(hidden_word)
    letters_to_guess = word[1:lenght-1]

    for i in range(lenght):
        if i != 0 and i != lenght - 1:
            hidden_word[i] = '-'

    return hidden_word, letters_to_guess

def reveal_letter(letter, word, hidden_word):

    for i in range(1, len(word) - 1):
        if letter == word[i] and hidden_word[i] == '-':
            hidden_word[i] = letter
    return hidden_word

def hangman():

    print('Enter the secret word:')
    word = list(input())
    
    hidden_word, letters_to_guess = hide_word(word)
    print(hidden_word)

    lives_remaining = 5
    
    while lives_remaining > 0 and '-' in hidden_word:

        letter = input()

        if letter in letters_to_guess:
            print('Another letter guessed!')
            hidden_word = reveal_letter(letter, word, hidden_word)
            while letter in letters_to_guess:
                letters_to_guess.remove(letter)
            print(hidden_word)
        else:
            print('No such letter or already guessed!')
            lives_remaining -= 1
            print(f'Lives remaining: {lives_remaining}')
        

    if '-' in hidden_word:
        print('You lose!')
    else:
        print('You win!')

hangman()