# 1. Write a function that sums only the digits in a string.

def sum_digits(string: str):
    sum = 0
    for symbol in string:
        if symbol.isdigit():
            sum += int(symbol)
    print(sum)

sum_digits('abca4')


# 2. Write a function that 'merges' two equal length numbers. 
# The merging operation adds the digits that are in the same positions and if the result is greater than 9, only the last digit remains. 
# So 1 + 2 = 3, but 8 + 5 = 3 also.

def merge(one: int, two: int):
    one = str(one) 
    two = str(two)
    new_number = ''
    for i in range(len(two)):
        number = f'{int(one[i]) + int(two[i])}'
        if int(number) >= 10:
            number = number[1]
        new_number += (number)
    print(new_number)

merge(123, 123)
merge(789, 123)

# 3. Write a function that counts the number of occurrences of a letter in a string.

def occurrences(letter:str, string:str):
    letter_ocurrence = 0
    for symbol in string:
        if letter == symbol:
            letter_ocurrence += 1
    return letter_ocurrence

x = occurrences('a', 'aaa') # x = 3
x = occurrences('a', 'aabb') # x = 2
x = occurrences('a', 'bbcc') # x = 0

# 4. Write a function that returns the letter which occurs more often than another letter in a text. Use the function from the previous exercise.

def stronger_letter(string: str, first: str, second: str):
    if occurrences(first, string) < occurrences(second, string):
        return second
    return first

print(stronger_letter('abca', 'a', 'b')) # x = 'a'
print(stronger_letter('abbca', 'c', 'b')) # x = 'b'
print(stronger_letter('aabbc', 'b', 'a')) # x = 'b' (return the first one in case of a tie)


# 5. Write a function that determines if three numbers (representing lengths of sides) can form a triangle. 
# To form a triangle the largest side must be smaller than the sum of the other two. 
# You do not know which of the three parameters will be the largest.

def can_form_triangle(a: int, b: int, c: int):
    a, b, c = sorted([a,b,c])

    return a + b > c



print(can_form_triangle(3,4,5)) # x = True
print(can_form_triangle(3,6,3)) # x = False
