from instruction_runner import execute_instruction

output = []
while True:
    line = input()
    if line.lower() == 'exit':
        break

    output.append((execute_instruction(line)))

print('\n'.join(output))
