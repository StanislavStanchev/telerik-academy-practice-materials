import console_dict


def execute_instruction(line):
    parts = line.split(':')
    instruction = parts[0].lower()

    if instruction == 'find':
        word = parts[1]
        entry = console_dict.find_word(word)

        if entry is None:
            return 'No such word'
        else:
            word, meaning = entry
            return f'{word}: "{meaning}"'
    elif instruction == 'add':
        word = parts[1]
        meaning = parts[2]
        console_dict.add_word(word, meaning)

        return 'Added word'
    elif instruction == 'update':
        word = parts[1]
        meaning = parts[2]
        if console_dict.find_word(word) is None:
            return 'No such word'
        else:
            console_dict.update_meaning(word, meaning)
            return 'Meaning updated'
    else:
        return 'No such instruction'
