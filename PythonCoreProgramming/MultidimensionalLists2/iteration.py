matrix = [[1, 2, 3], [4, 5, 6]]

# rows first
for row in matrix:
    for column in row:
        print(column)

# columns first
for j in range(len(matrix[0])):
    for i in range(len(matrix)):
        print(matrix[i][j])

# diagonal
for i in range(len(matrix)):
    print(matrix[i][i])